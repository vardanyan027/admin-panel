@extends('home')
@section('content')

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Edit Company
    </button>

    <!-- Company Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="company-name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="company-name" value="{{$company->name}}">
                        </div>
                        <div class="form-group">
                            <label for="company-email" class="col-form-label">Email:</label>
                            <input type="email" name="email" class="form-control" id="company-email" value="{{$company->email}}">
                        </div>
                        <div class="form-group">
                            <label for="company-logo" class="col-form-label">Logo:</label>
                            <input type="file" name="logo" class="form-control" id="company-logo">
                        </div>
                        <div class="form-group">
                            <label for="company-website" class="col-form-label">Website:</label>
                            <input type="text" name="website" class="form-control" id="company-website" value="{{$company->website}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" onclick="companyEdit({{$company->id}})">Save changes</button>
                    </div>
            </div>
        </div>
    </div>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalEmployee">
        Create Employee
    </button>

    <!-- Employee Modal -->
    <div class="modal fade" id="exampleModalEmployee" tabindex="-1" aria-labelledby="exampleModalEmployeeLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/employee" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="employee-first-name" class="col-form-label">First Name:</label>
                            <input type="text" name="first_name" class="form-control" id="employee-first-name">
                        </div>
                        <div class="form-group">
                            <label for="employee-last-name" class="col-form-label">Last Name:</label>
                            <input type="text" name="last_name" class="form-control" id="employee-last-name">
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="company_id" class="form-control" id="employee-company-id" value="{{$company->id}}">
                        </div>
                        <div class="form-group">
                            <label for="employee-email" class="col-form-label">Email:</label>
                            <input type="email" name="email" class="form-control" id="employee-email">
                        </div>
                        <div class="form-group">
                            <label for="employee-phone" class="col-form-label">Phone:</label>
                            <input type="text" name="phone" class="form-control" id="employee-phone">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Full name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $employee)
                <tr>
                    <th scope="row">{{$employee->id}}</th>
                    <td>{{$employee->first_name}} {{$employee->last_name}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalEmployee-{{$employee->id}}">
                        Edit Employee
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalEmployee-{{$employee->id}}" tabindex="-1" aria-labelledby="exampleModalEmployeeLabel-{{$employee->id}}" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                    <div class="modal-body">
                                        <input type="hidden" id="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="employee-first-name" class="col-form-label">First Name:</label>
                                            <input type="text" name="first_name" class="form-control" id="employee-{{$employee->id}}-first-name" value="{{$employee->first_name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="employee-last-name" class="col-form-label">Last Name:</label>
                                            <input type="text" name="last_name" class="form-control" id="employee-{{$employee->id}}-last-name" value="{{$employee->last_name}}">
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="company_id" class="form-control" id="employee-company-id" value="{{$company->id}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="employee-email" class="col-form-label">Email:</label>
                                            <input type="email" name="email" class="form-control" id="employee-{{$employee->id}}-email" value="{{$employee->email}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="employee-phone" class="col-form-label">Phone:</label>
                                            <input type="text" name="phone" class="form-control" id="employee-{{$employee->id}}-phone" value="{{$employee->phone}}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary" onclick="handleSubmitForm({{$employee->id}})" data-id="{{$employee->id}}">Save</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                        <div>
                            <form action="{{route('employee.destroy', ['employee' => $employee])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary">
                                    Delete Employee
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script type="text/javascript">
            function handleSubmitForm(id) {
                const formData = {
                    first_name: $("#employee-" + id + "-first-name").val(),
                    last_name: $("#employee-" + id + "-last-name").val(),
                    email: $("#employee-" + id + "-email").val(),
                    phone: $("#employee-" + id + "-phone").val()
                };

                $.ajax({
                    type: "PUT",
                    url: '/employee/' + id,
                    data: formData,
                    success: function (response) {
                        console.log("Request successful:", response);
                        $("#exampleModalEmployee-"+id).modal("hide");
                    },
                    error: function (error) {
                        console.error(error);
                    }
                });
            }

            function companyEdit(id) {
                const formData = new FormData();

                formData.append("name", $("#company-name").val());
                formData.append("email", $("#company-email").val());
                formData.append("website", $("#company-website").val());

                const logoFileInput = document.getElementById("company-logo");
                if (logoFileInput.files.length > 0) {
                    formData.append("logo", logoFileInput.files[0]);
                }


                $.ajax({
                    type: "PUT",
                    url: '/company/' + id,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        console.log("Request successful:", response);
                        $("#exampleModalEmployee-"+id).modal("hide");
                    },
                    error: function (error) {
                        console.error(error);
                    }
                });
            }
    </script>

@endsection
