@extends('home')
@section('content')

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Create Company
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/company" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="company-name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="company-name">
                        </div>
                        <div class="form-group">
                            <label for="company-email" class="col-form-label">Email:</label>
                            <input type="email" name="email" class="form-control" id="company-email">
                        </div>
                        <div class="form-group">
                            <label for="company-logo" class="col-form-label">Logo:</label>
                            <input type="file" name="logo" class="form-control" id="company-logo">
                        </div>
                        <div class="form-group">
                            <label for="company-website" class="col-form-label">Website:</label>
                            <input type="text" name="website" class="form-control" id="company-website">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Logo</th>
                <th scope="col">Website</th>
            </tr>
        </thead>
        <tbody>
            @foreach($companies as $company)
                <tr>
                        <th scope="row">{{$company->id}}</th>
                        <td>
                            <a href="{{route('company.show', ['company' => $company])}}">
                                {{$company->name}}
                            </a>
                        </td>
                        <td>{{$company->email}}</td>
                        <td>{{$company->logo}}</td>
                        <td>{{$company->website}}</td>
                    <td>
                        <div>
                            <form action="{{route('company.destroy', ['company' => $company])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary">
                                    Delete Company
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
