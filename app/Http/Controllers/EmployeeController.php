<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUpdateEmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class EmployeeController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $employee = Employee::where('id', $id)->first();

        return view('employee/index', [
            'employee' => $employee,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUpdateEmployeeRequest  $request
     * @return Application|Redirector|RedirectResponse
     */
    public function store(CreateUpdateEmployeeRequest $request)
    {
        $company = Company::where('id', $request->get('company_id'))->first();
        $employee = new Employee();

        $employee->first_name = $request->get('first_name');
        $employee->last_name = $request->get('last_name');
        $employee->email = $request->get('email');
        $employee->phone = $request->get('phone');

        $company->employees()->save($employee);

        return redirect(route('company.show', ['company' => $company]));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  CreateUpdateEmployeeRequest  $request
     * @param  int  $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(CreateUpdateEmployeeRequest $request, int $id)
    {
        $employee = Employee::where('id', $id)->first();

        $employee->first_name = $request->get('first_name');
        $employee->last_name = $request->get('last_name');
        $employee->email = $request->get('email');
        $employee->phone = $request->get('phone');

        $employee->save();
        $company = $employee->company;

        return redirect(route('company.show', ['company' => $company]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        try {
            Employee::where('id', $id)->delete();
            return back();
        } catch (\Exception $ex) {
            return back();
        }
    }
}
