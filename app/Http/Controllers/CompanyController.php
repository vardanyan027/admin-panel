<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUpdateCompanyRequest;
use App\Models\Company;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('company', [
            'companies' => $companies
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUpdateCompanyRequest $request
     * @return Application|Redirector|RedirectResponse
     */
    public function store(CreateUpdateCompanyRequest $request)
    {
        $company =  new Company();
        $logo = $request->file('logo')->getClientOriginalName();
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->logo = $logo;
        $company->website = $request->get('website');

        Storage::disk('local')->put('public/'.$logo, file_get_contents($request->file('logo')));

        $company->save();

        return redirect('/company');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $company = Company::where('id', $id)->first();

        return view('company/index', [
            'company' => $company,
            'employees' => $company->employees
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateUpdateCompanyRequest  $request
     * @param int $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(CreateUpdateCompanyRequest $request, int $id)
    {
        $company = Company::where('id', $id)->first();
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->website = $request->get('website');

        $company->save();

        return redirect('/company/' . $company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(int $id)
    {
        try {

            Company::where('id', $id)->delete();
            return redirect('/company');
        } catch (\Exception $ex) {
            return redirect('/company');
        }
    }
}
